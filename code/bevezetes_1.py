import turtle

# Vászon, amire rajzolni fogunk (mérete és színe lényeges)
turtle.screensize(800,600)
#turtle.bgcolor("saddle brown")
#turtle.bgcolor("plum4")
#turtle.bgcolor("#ff66ff") # R,G,B komponensekből is meg lehet adni
turtle.bgcolor("aqua")

# Teknős, amivel rajzolni fogunk 
# (alakja, színe, milyen színű csíkot húzzon maga után)

# Teknős lehetséges alakjai: "arrow", "turtle", "circle", "square", "triangle", "classic"
turtle.shape("turtle")

turtle.color("red")
turtle.pencolor("blue")

# Kezdjünk el rajzolni a teknősünkkel a (0,0) pontból indulva!
# (Lehetne máshonnan is, majd látni fogjuk.)
turtle.forward(100)

turtle.right(90)
turtle.forward(100)

turtle.right(90)
turtle.forward(100)

turtle.right(90)
turtle.forward(100)

# Háztető a négyzetre: egyenlő oldalú háromszög
# Miért 30 fok?
turtle.right(30)
turtle.forward(100)

# Miért 120 fok?
turtle.right(120)
turtle.forward(100)

# Program vége, de némely könyezetben azonnal be is zárná az ablakot.
# Ezért megállítjuk, és a konzol ablakban leütött Enter-re várunk.
# input("Program vége, nyomj Enter-t...")
