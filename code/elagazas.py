"""
Elágazás példaprogram.
Egyszerű if...else, 2 ágú.
"""
szoveg= input("Gépelj be egy számot! ")
x = float(szoveg)
print(x)

if x<0:
    print( "Negatív" )
    print( "if ág vége." )
else:
    print( "Nemnegatív" )
    print( "else ág vége." )

print ( "Program vége" )

"""
Több ágú, if...else: if...-fel.
Látszik, hogy ronda...
"""
szin = "piros"

if szin == "piros":
    #mindegy
    pass
else:
    if szin == "pirossarga":
        pass
    else:
        if szin == "sarga":
            pass
        else:
            if szin == "zold":
                pass
            else:
                pass
            
"""
Az elif kulcsszó bevezetése.
Szintaktikus cukorka az egyre mélyebb tabulálások elkerülésére.
"""

if szin == "piros":
    pass
elif szin == "pirossarga":
    pass
elif szin == "sarga":
    pass
elif szin == "zold":
    pass


"""
    Kiszámolja egy szám abszolút értékét.
    Az elágazás fogalmának bevezetése.
    Átvezetés a kivételkezeléshez.
"""
szam_str = input ("Írj be egy számot, megmondom az abszolút értékét! ")
szam = float(szam_str)

##if szam < 0 :
##    szam_abs = -szam
##else:
##    if szam == 0:
##        szam_abs = szam
##    else:
##        if szam > 0:
##            szam_abs = szam
##        else:
##            # mi van, ha nem is szám?
##            pass

if szam < 0:
    szam_abs = -szam
elif szam == 0:
    szam_abs = szam
elif szam > 0:
    szam_abs = szam
else:
    # mi van, ha nem is szám?
    # Sajnos nem ilyen egyszerű, nem ide fut be
    # Hanem ValueError kivételt dob...
    pass

print ("Az abszolút értéke: " , szam_abs)
