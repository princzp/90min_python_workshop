# Számkitalálós játék

# A gép kitalál egy véletlen számot 1 és 10 között
from random import randint
gondolt_szam = randint(1,10)

print ("Gondoltam egy számot 1 és 10 között. Találd ki!")

# Ezt éles játékban ki lehet venni
print ("Súgok: a %d számra gondoltam..." % gondolt_szam)

tippek_meg = 3

while tippek_meg > 0:
    print("Mi a tipped? (Van még " + str(tippek_meg) + "...)")
    jatekos_tippje = int(input())

    if gondolt_szam == jatekos_tippje:
        print("Gratulálok! Nyertél!")
        break
    elif gondolt_szam > jatekos_tippje:
        print("Nagyobb számra gondoltam.")
    elif gondolt_szam < jatekos_tippje:
        print("Kisebb számra gondoltam.")

    if tippek_meg == 1 and gondolt_szam != jatekos_tippje:
       print("Vége, nincs több tipped.")

    tippek_meg = tippek_meg - 1
print ("...Vége a játéknak...")
